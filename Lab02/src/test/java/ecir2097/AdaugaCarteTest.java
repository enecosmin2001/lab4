package ecir2097;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

public class AdaugaCarteTest {
    private CartiRepo cartiRepo = new CartiRepo();
    @Test
    public void TC01_EC(){
        Carte carte = new Carte("Ion", "Liviu Rebreanu", 1920, "Realism Obiectiv");
        Integer repo_size = this.cartiRepo.size();
        try {
            this.cartiRepo.adaugaCarte(carte);
        } catch (RepositoryException e) {
            assertTrue(false);
        }
        assert (this.cartiRepo.size() == repo_size + 1);
        assertTrue(true);
    }

    @Test
    public void TC06_EC(){
        Carte carte = new Carte("Ion", "Liviu", 1500, "Gen1");
        try {
            this.cartiRepo.adaugaCarte(carte);
            assertTrue(false);
        } catch (RepositoryException e) {
            assertTrue(true);
        }
        Carte carte1 = new Carte("Ion2", "Liviu", 2020, "Gen1");
        try {
            this.cartiRepo.adaugaCarte(carte1);
            assertTrue(false);
        } catch (RepositoryException e) {
            assertTrue(true);
        }
        assertTrue(true);
    }

    @Test
    public void TC07_EC(){
        Carte carte = new Carte("", "Liviu", 1999, "Gen1");
        try {
            this.cartiRepo.adaugaCarte(carte);
            assertTrue(false);
        } catch (RepositoryException e) {
            assertTrue(true);
        }
        assertTrue(true);
    }

    @Test
    public void TC08_EC(){
        Carte carte = new Carte("Autor", "", 1999, "Gen1");
        try {
            this.cartiRepo.adaugaCarte(carte);
            assertTrue(false);
        } catch (RepositoryException e) {
            assertTrue(true);
        }
        assertTrue(true);
    }

    @Test
    public void TC09_EC(){
        Carte carte = new Carte("Autor", "Sal", 1999, "");
        try {
            this.cartiRepo.adaugaCarte(carte);
            assertTrue(false);
        } catch (RepositoryException e) {
            assertTrue(true);
        }
        assertTrue(true);
    }

    @Test
    public void TC09_BVA(){
        Carte carte = new Carte("Title", "D", 1999, "sal");
        try {
            this.cartiRepo.adaugaCarte(carte);
            assertTrue(true);
        } catch (RepositoryException e) {
            assertTrue(false);
        }
        assertTrue(true);
    }

    @Test
    public void TC14_BVA(){
        Integer an_ap = 1701;
        Carte carte = new Carte("Autor", "Sal", an_ap, "sal");
        try {
            this.cartiRepo.adaugaCarte(carte);
            assertTrue(true);
        } catch (RepositoryException e) {
            assertTrue(false);
        }
        assertTrue(true);
    }

    @Test
    public void TC16_BVA(){
        Integer an_ap = Integer.MAX_VALUE -1;
        Carte carte = new Carte("Autor", "Sal", an_ap, "GEN");
        try {
            this.cartiRepo.adaugaCarte(carte);
            assertTrue(false);
        } catch (RepositoryException e) {
            assertTrue(true);
        }
        assertTrue(true);
    }

    @Test
    public void TC18_BVA(){
        Integer an_ap = Integer.MAX_VALUE;
        Carte carte = new Carte("Autor", "Sal", an_ap, "GEN");
        try {
            this.cartiRepo.adaugaCarte(carte);
            assertTrue(false);
        } catch (RepositoryException e) {
            assertTrue(true);
        }
        assertTrue(true);
    }

}
