package ecir2097;

import java.util.ArrayList;
import java.util.List;

public class CartiRepo {
    private List<Carte> carti;
    private CartiValidator validator;
    public CartiRepo() {
        this.carti = new ArrayList<Carte>();
        this.validator = new CartiValidator();
    }

    public void adaugaCarte(Carte carte) throws RepositoryException {
        try {
            this.validator.valideazaCarte(carte);
        } catch (ValidareException e) {
            throw new RepositoryException(e.getMessage());
        }
        if (this.gasesteCarte(carte.getTitlu()) != null){
            throw new RepositoryException("Cartea exista deja in baza de date!");
        }
        this.carti.add(carte);
    }

    private Carte gasesteCarte(String titlu){
        for (Carte c:
             this.carti) {
            if (c.getTitlu() == titlu) {
                return c;
            }
        }
        return null;
    }

    public Integer size(){
        return this.carti.size();
    }
}
