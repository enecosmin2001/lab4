package ecir2097;

public class RepositoryException extends Exception{
    public RepositoryException(String message) {
        super(message);
    }
}
