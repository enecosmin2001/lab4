package ecir2097;

public class CartiValidator {
    public CartiValidator() { }

    public void valideazaCarte(Carte carte) throws ValidareException {
        if (carte.getTitlu() == ""){
            throw new ValidareException("Cartea trebuie sa aiba un titlu!");
        }
        if (carte.getAutor() == ""){
            throw new ValidareException("Cartea trebuie sa aiba un autor!");
        }
        if (carte.getAn_apartie() < 1700 || carte.getAn_apartie() > 2019){
            throw new ValidareException("Anul aparitiei nu este acceptat!");
        }
        if (carte.getGen() == ""){
            throw new ValidareException("Cartea trebuie sa aiba un gen!");
        }
    }
}
