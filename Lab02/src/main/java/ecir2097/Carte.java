package ecir2097;

public class Carte {
    private String titlu;
    private String autor;
    private Integer an_apartie;
    private String gen;

    public Carte(String titlu, String autor, Integer an_apartie, String gen) {
        this.titlu = titlu;
        this.autor = autor;
        this.an_apartie = an_apartie;
        this.gen = gen;
    }

    public String getTitlu() {
        return titlu;
    }

    public void setTitlu(String titlu) {
        this.titlu = titlu;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public Integer getAn_apartie() {
        return an_apartie;
    }

    public void setAn_apartie(Integer an_apartie) {
        this.an_apartie = an_apartie;
    }

    public String getGen() {
        return gen;
    }

    public void setGen(String gen) {
        this.gen = gen;
    }
}
